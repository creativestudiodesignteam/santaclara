<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home header-localizacao">
        <?php 
        $label = "Localizacao";
        include("includes/menu.php"); ?>
        
        <?php 
        $title = "O Resort > Localização";
        include("includes/top-bar.php"); ?>
    </header>

    <section class="localization-01">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Duis vitae aliquet ante, vitae accumsan libero. </h1>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. 
                    </p>

                    <?php include("includes/map.php")?>
                    <a href="#." class="btn-outline-green">Traçar rota até o hotel</a>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-xl-12 text-center">
                    <h1 class="title">A Região</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-6">
                    <p class="text">Em meados do século XIX, pequenas choupanas foram surgindo na região e, mais tarde, formaram um povoado denominado Bebedouro, atualmente município. Um dos mais antigos moradores do lugar, capitão José Modesto de Abreu, doou uma gleba de terras situada na Serra dos Dourados, onde o capitão José Sijus ergueu o primeiro rancho.</p>
                    <p class="text">
                        Uma capela foi construída e a partir de 1880 algumas famílias foram-se fixando ao seu redor, formando a povoação de São João Batista dos Dourados, cujo nome homenageia o Santo padroeiro e identifica a localização do povoado.</p>
                    </p>
                </div>

                <div class="col-xl-6">
                    <p class="text">Passada uma década, as duas povoações vizinhas, São João dos Dourados e Bebedouro, reivindicavam a elevação à categoria de Distrito de Paz, tendo alcançado melhor êxito, em 1891, a de São João Batista dos Dourados.</p>

                    <p class="text">
                    Essa pequena cidade cafeeira preservada no tempo com sua arquitetura histórica, mantém todo seu charme original.
                    </p>

                    <p class="text">
                    Localiza-se em uma estação climática a 706 metros acima do mar, com clima ameno e agradável o ano todo.                        
                    </p>
                </div>
            </div>
        </div>
    </section>

    
    <?php include("includes/gallery.php")?>
    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html> 