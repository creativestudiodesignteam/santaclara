<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home">
        <?php 
        $label = "O Resort";
        include("includes/menu.php"); ?>
        
        <?php 
        $title = "O Resort";
        include("includes/top-bar.php"); ?>
    </header>

    <section class="resort">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Duis vitae aliquet ante, vitae accumsan libero. </h1>
                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. </p>
                    
                    <div class="card-deck">
                        <a href="#" class="card position-relative">
                            <img class="card-img-top" src="assets/images/localizacao.jpg" class="img-fluid"/>

                            <div class="card-body">
                                <h1 class="card-title m-0">Localização</h1>
                            </div>
                        </a>

                        <a href="#" class="card position-relative">
                            <img class="card-img-top" src="assets/images/infra.jpg" class="img-fluid"/>

                            <div class="card-body">
                                <h1 class="card-title m-0">Infraestrutura</h1>
                            </div>
                        </a>

                        <a href="#" class="card position-relative">
                            <img class="card-img-top" src="assets/images/cuidados.jpg" class="img-fluid"/>

                            <div class="card-body">
                                <h1 class="card-title m-0">Cuidados para familia</h1>
                            </div>
                        </a>

                        <a href="#" class="card position-relative">
                            <img class="card-img-top" src="assets/images/sust.jpg" class="img-fluid"/>

                            <div class="card-body">
                                <h1 class="card-title m-0">Sustentabilidade e Social</h1>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include("includes/accommodations.php")?>
    <?php include("includes/activities-leisure.php")?>
    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html>