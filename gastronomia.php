<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home header-gastronomia text-center">
        <?php 
        $label = "Gastronomia";
        include("includes/menu.php"); ?>
        

        <?php 
        $title = "Gastronomia";
        include("includes/top-bar.php"); ?>
        <a href="#." data-toggle="modal" data-target="#youtubeVideo"><img src="assets/images/play.png" width="80px" class="img-fluid play"></a>
    </header>

    <section class="gastronomia">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Duis vitae aliquet ante, vitae accumsan libero. </h1>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. 
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="gallery-gastronomia">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <div class="gallery">
                        <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                        <div class="clear"></div>

                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                        <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="youtubeVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/i2JmVSqxHrw" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <a href="#" class="btn-outline-green" data-dismiss="modal">Fechar vídeo</a>
                </div>
            </div>
        </div>
    </div>

    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html> 