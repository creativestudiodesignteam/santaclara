<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home header-aux">
        <?php 
        $label = "Cavalgada";
        include("includes/menu.php"); ?>
        
        <nav aria-label='breadcrumb' class='d-none d-xl-block' style="position: absolute;z-index: 2;left: 0;right: 0;margin-top: 20px;">
            <ol class='breadcrumb d-flex justify-content-center bg-transparent'>
                <li class='breadcrumb-item'><a href='index.php'>Home</a></li>
                <li class='breadcrumb-item active' aria-current='page'>Para todas as idades</li>
                <li class='breadcrumb-item active' aria-current='page'>Cavalgada</li>
            </ol>
        </nav>

        <?php 
        $bgSlide = "cavalgada.jpg";
        $titleSlide = "";
        include("includes/slide.php"); ?>
    </header>

    <section class="int-ativ">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Duis vitae aliquet ante, vitae accumsan libero. </h1>
                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. </p>
                </div>
            </div>
        </div>
    </section>

    <section class="programacao-alg">
        <div class="container">
        <div class="row">
            <div class="col-xl-12 text-center">
                <h1 class="title">programação</h1>
                <div class="swiper-container swiper-program">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="row">
                                <div class="col-xl-12 text-center">
                                    <p class="week">Segunda-feira<br><span class="data">26/08</span></p>
                                </div>
                            </div>
                            <div class="center-elements">
                                <div class="box-one-program">
                                    <div class="f-aux">
                                        <div class="text-center">
                                            <h3 class="title-day">manhã</h3>
                                            
                                            <a href="#." class="box-program mb-2">
                                                <span class="hour">08:00</span>
                                                <p class="aula m-0">Aula de equitação</p>
                                                <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                            </a>
                                            <a href="#." class="box-program mb-2">
                                                <span class="hour">08:00</span>
                                                <p class="aula m-0">Aula de equitação</p>
                                                <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                            </a>
                                            <a href="#." class="box-program mb-2">
                                                <span class="hour">08:00</span>
                                                <p class="aula m-0">Aula de equitação</p>
                                                <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
    
                                <div class="box-one-program">
                                    <div class="f-aux">
                                        <div class="text-center">
                                            <h3 class="title-day">tarde</h3>
                                            
                                            <a href="#." class="box-program mb-2">
                                                <span class="hour">08:00</span>
                                                <p class="aula m-0">Aula de equitação</p>
                                                <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                            </a>
                                            <a href="#." class="box-program mb-2">
                                                <span class="hour">08:00</span>
                                                <p class="aula m-0">Aula de equitação</p>
                                                <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                            </a>
                                            <a href="#." class="box-program mb-2">
                                                <span class="hour">08:00</span>
                                                <p class="aula m-0">Aula de equitação</p>
                                                <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
    
                                <div class="box-one-program">
                                    <div class="f-aux">
                                        <div class="text-center">
                                            <h3 class="title-day">noite</h3>
                                            
                                            <a href="#." class="box-program mb-2">
                                                <span class="hour">08:00</span>
                                                <p class="aula m-0">Aula de equitação</p>
                                                <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                            </a>
                                            <a href="#." class="box-program mb-2">
                                                <span class="hour">08:00</span>
                                                <p class="aula m-0">Aula de equitação</p>
                                                <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                            </a>
                                            <a href="#." class="box-program mb-2">
                                                <span class="hour">08:00</span>
                                                <p class="aula m-0">Aula de equitação</p>
                                                <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-counter d-none"></div>
                    <div class="arrows">
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
    

                <a href="programacao.php" class="btn-outline-green mt-4 mb-4">Ver toda programação</a>
            </div>
        </div>
        </div>
    </section>

    <?php include("includes/map.php")?>
    <?php include("includes/gallery.php")?>
    <?php include("includes/testmonials.php")?>
    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html>