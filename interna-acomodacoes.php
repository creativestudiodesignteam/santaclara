<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home header-aux">
        <?php 
        $label = "Suite Plus";
        include("includes/menu.php"); ?>
        
        <!-- <?php 
        $title = "Acomodações > Suite Plus";
        include("includes/top-bar.php"); ?> -->

        <nav aria-label='breadcrumb' class='d-none d-xl-block' style="position: absolute;z-index: 2;left: 0;right: 0;margin-top: 20px;">
            <ol class='breadcrumb d-flex justify-content-center bg-transparent'>
                <li class='breadcrumb-item'><a href='index.php'>Home</a></li>
                <li class='breadcrumb-item active' aria-current='page'>Acomodações</li>
                <li class='breadcrumb-item active' aria-current='page'>Suite Plus</li>
            </ol>
        </nav>
        
        <?php 
        $bgSlide = "suite.jpg";
        $titleSlide = "";
        include("includes/slide.php"); ?>
    </header>

    <section class="int-acom">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Duis vitae aliquet ante, vitae accumsan libero. </h1>
                    <div class="row no-gutters text-center icons mt-3">
                        <div class="col-12">
                            <ul class="list-unstyled list-inline">
                                <li class="list-inline-item">
                                    <a href="#."><i class="flaticon-bathtub"></i><br>Banheiro</a>
                                </li>

                                <li class="list-inline-item">
                                    <a href="#."><i class="flaticon-fireplace"></i><br>lareira</a>
                                </li>

                                <li class="list-inline-item">
                                    <a href="#."><i class="flaticon-phone-call"></i><br>Telefone</a>
                                </li>

                                <li class="list-inline-item">
                                    <a href="#."><i class="flaticon-tv"></i><br>TV</a>
                                </li>

                                <li class="list-inline-item">
                                    <a href="#."><i class="fas fa-wifi"></i><br>Wi-fi</a>
                                </li>

                                <li class="list-inline-item">
                                    <a href="#."><i class="flaticon-group-profile-users"></i><br>Até 6 pessoas</a>
                                </li>


                            </ul>
                        </div>
                    </div>
                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. </p>
                    <a href="acomodacoes.php" class="btn-outline-green mt-4">Viaje leve</a>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/map.php")?>

    <section class="baby">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Cuidados para seu bebê </h1>
                    <p class="description">Toda acomodação possui estrutura para seu bebê. Requisite na reserva.</p>

                    <div class="swiper-container swiper-baby position-relative mt-4">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide" style="background-image:url('assets/images/baby-01.jpg');">
                                <h1 class="title-baby">
                                    Cuidados para seu bebê
                                </h1>
                            </div>

                            <div class="swiper-slide" style="background-image:url('assets/images/baby-01.jpg');">
                                <h1 class="title-baby">
                                    Cuidados para seu bebê
                                </h1>
                            </div>

                            <div class="swiper-slide" style="background-image:url('assets/images/baby-01.jpg');">
                                <h1 class="title-baby">
                                    Cuidados para seu bebê
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-pagination-baby"></div>
                </div>
            </div>
        </div>
    </section>

    <?php include("includes/gallery.php")?>
    <?php include("includes/testmonials.php")?>
    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html>