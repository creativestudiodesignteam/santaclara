<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home header-eventos">
        <?php 
        $label = "Eventos";
        include("includes/menu.php"); ?>
        
        <?php 
        $title = "Eventos";
        include("includes/top-bar.php"); ?>
    </header>

    <section class="eventos-01">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Duis vitae aliquet ante, vitae accumsan libero. </h1>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. 
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="about-eventos-01">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h2 class="title-master">Conheça a infraestrutura</h2>
                    <div class="swiper-container swiper-eventos-01">
                        <div class="swiper-counter"></div>
                        <div class="arrows">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>

                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                               <div class="gallery-infra">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12 text-center">
                                                <h1 class="title">Outdoor</h1>
                                                <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. </p>
                                                <div class="gallery">
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>

                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                               <div class="gallery-infra">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12 text-center">
                                                <h1 class="title">Outdoor</h1>
                                                <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. </p>
                                                <div class="gallery">
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>

                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                               <div class="gallery-infra">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12 text-center">
                                                <h1 class="title">Outdoor</h1>
                                                <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. </p>
                                                <div class="gallery">
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>

                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contato">
        <div class="container">
            <div class="row mt-5">
                <div class="col-xl-12 text-center">
                    <h2 class="title">Entre em contato</h2>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-xl-4 offset-xl-2 box">
                    <div class="row">
                        <div class="col-xl-2 align-self-center">
                            <i class="fab fa-whatsapp"></i>
                        </div>
                        <div class="col-xl-10 align-self-center">
                            <a href="#.">
                                <p class="title m-0">Whatsapp</p>    
                                <p class="number m-0">(16) 98174-7159</p>    
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 box">
                    <div class="row">
                        <div class="col-xl-2 align-self-center">
                            <i class="fas fa-envelope"></i>
                        </div>
                        <div class="col-xl-10 align-self-center">
                            <a href="#.">
                                <p class="title m-0">E-mail</p>    
                                <p class="number m-0">contato@santaclaraecoresorts.com.br</p>    
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="clientes">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h2 class="title">Nossos clientes</h2>
                    <a href="#.">
                        <img src="assets/images/clientes/01.png" width="150px" class="img-fluid"/>
                    </a>

                    <a href="#.">
                        <img src="assets/images/clientes/02.png" width="150px" class="img-fluid"/>
                    </a>

                    <a href="#.">
                        <img src="assets/images/clientes/03.png" width="150px" class="img-fluid"/>
                    </a>

                    <a href="#.">
                        <img src="assets/images/clientes/04.png" width="150px" class="img-fluid"/>
                    </a>

                    <a href="#.">
                        <img src="assets/images/clientes/05.png" width="150px" class="img-fluid"/>
                    </a>
                </div>
            </div>
        </div>
    </section>

    

    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html> 