<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <?php include("includes/load.php"); ?>
    <header class="header-home">
        <?php 
        $label = "Menu";
        include("includes/menu.php"); ?>
            
        <?php 
        $bgSlide = "slide-01.jpg";
        $titleSlide = "Lindo final<br>de semana";
        include("includes/slide.php"); ?>
    </header>

    <section class="seals" id="seals">
        <div class="container">
            <div class="row">
                <div class="col-4 col-xl-2 offset-xl-3 align-self-center">
                    <a href="#.">
                        <img src="assets/images/selo-01.png" class="img-fluid"/>
                    </a>
                </div>
                <div class="col-4 col-xl-2 align-self-center">
                    <a href="#.">
                        <img src="assets/images/selo-02.png" class="img-fluid"/>
                    </a>
                </div>
                <div class="col-4 col-xl-2 align-self-center">
                    <a href="#.">
                        <img src="assets/images/selo-03.png" class="img-fluid"/>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <?php include("includes/accommodations.php")?>
    <?php include("includes/activities-leisure.php")?>
    <?php include("includes/testmonials.php")?>
    <?php include("includes/gallery.php")?>

    <section class="events">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 text-center">
                    <h2 class="title-master">Eventos</h2>
                    <div class="swiper-container swiper-events">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="assets/images/convencoes.jpg" class="img-fluid">
                                <h1 class="title">Convenções</h1>
                            </div>

                            <div class="swiper-slide">
                                <img src="assets/images/convencoes.jpg" class="img-fluid">
                                <h1 class="title">Convenções</h1>
                            </div>

                            <div class="swiper-slide">
                                <img src="assets/images/convencoes.jpg" class="img-fluid">
                                <h1 class="title">Convenções</h1>
                            </div>
                        </div>
                        <div class="swiper-counter"></div>
                        <div class="arrows">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>

                    <a href="eventos.php" class="btn-outline-green mt-4 mb-4 d-block d-xl-none">Ver todos os eventos</a>
                </div>

                <div class="col-xl-6 text-center">
                    <h2 class="title-master">Sustentabilidade e Social</h2>
                    <div class="swiper-sust">
                        <img src="assets/images/criancas.jpg" class="img-fluid">
                        <p class="description">No Santa Clara Eco Resort a preocupação com o meio ambiente vai muito além de ações pontuais: é parte do DNA do resort</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="know">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Conheça também</h1>
                    <div class="card-deck">
                        <div class="row">
                            <div class="col-xl-4 d-none d-xl-block">
                                <a href="#" class="card position-relative">
                                    <img class="card-img-top" src="assets/images/criancas.jpg" class="img-fluid"/>
                                </a>
                            </div>

                            <div class="col-xl-4">
                                <a href="#" class="card position-relative">
                                    <img class="card-img-top" src="assets/images/santa.jpg" class="img-fluid"/>
                                </a>
                            </div>

                            <div class="col-xl-4 d-none d-xl-block">
                                <a href="#" class="card position-relative">
                                    <img class="card-img-top" src="assets/images/criancas.jpg" class="img-fluid"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html>