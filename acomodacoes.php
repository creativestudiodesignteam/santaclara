<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home">
        <?php 
        $label = "Acomodações";
        include("includes/menu.php"); ?>
        
        <?php 
        $title = "Acomodações";
        include("includes/top-bar.php"); ?>
    </header>

    <section class="accommodations-01">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <p class="text">Filtre por capacidade da acomodação:</p>
                    <div class="form-group" id="rating-ability-wrapper">
                        <label class="control-label" for="rating">
                            <span class="field-label-info"></span>
                            <input type="hidden" id="selected_rating" name="selected_rating" value="" required="required">
                        </label>

                        <button type="button" class="btnrating" data-attr="1" id="rating-star-1">
                            <i class="fas fa-user"></i>
                        </button>

                        <button type="button" class="btnrating" data-attr="2" id="rating-star-2">
                            <i class="fas fa-user"></i>
                        </button>

                        <button type="button" class="btnrating" data-attr="3" id="rating-star-3">
                            <i class="fas fa-user"></i>
                        </button>

                        <button type="button" class="btnrating" data-attr="4" id="rating-star-4">
                            <i class="fas fa-user"></i>
                        </button>
                        
                        <button type="button" class="btnrating" data-attr="5" id="rating-star-5">
                            <i class="fas fa-user"></i>
                        </button>

                        <button type="button" class="btnrating" data-attr="5" id="rating-star-5">
                            <i class="fas fa-user"></i>
                        </button>
                    </div>
                    <div class="card-deck">
                        <div class="row">
                            <div class="col-xl-4">
                                <a href="interna-acomodacoes.php" class="card position-relative">
                                    <img class="card-img-top" src="assets/images/acomodacao-01.jpg" class="img-fluid"/>
                                    
                                    <div class="row no-gutters icons">
                                        <div class="col-12">
                                            <i class="flaticon-bathtub"></i>
                                            <i class="flaticon-snowflake"></i>
                                            <i class="flaticon-dryer"></i>
                                        </div>
                                    </div>
        
                                    <div class="card-body">
                                        <h1 class="card-title m-0">Apto vila do lago</h1>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-4">
                                <a href="interna-acomodacoes.php" class="card position-relative">
                                    <img class="card-img-top" src="assets/images/acomodacao-02.jpg" class="img-fluid"/>
                                    
                                    <div class="row no-gutters icons">
                                        <div class="col-12">
                                            <i class="flaticon-bathtub"></i>
                                            <i class="flaticon-snowflake"></i>
                                            <i class="flaticon-dryer"></i>
                                        </div>
                                    </div>
        
                                    <div class="card-body">
                                        <h1 class="card-title m-0">Suite Santa Clara</h1>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-4">
                                <a href="interna-acomodacoes.php" class="card position-relative">
                                    <img class="card-img-top" src="assets/images/acomodacao-01.jpg" class="img-fluid"/>
                                    
                                    <div class="row no-gutters icons">
                                        <div class="col-12">
                                            <i class="flaticon-bathtub"></i>
                                            <i class="flaticon-snowflake"></i>
                                            <i class="flaticon-dryer"></i>
                                        </div>
                                    </div>
        
                                    <div class="card-body">
                                        <h1 class="card-title m-0">Chalé Luxo Plus</h1>
                                    </div>
                                </a>
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/activities-leisure.php")?>
    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html>