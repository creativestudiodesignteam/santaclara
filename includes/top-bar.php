<nav aria-label="breadcrumb" class="d-none d-xl-block">
    <ol class="breadcrumb d-flex justify-content-center bg-transparent">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $title?></li>
    </ol>
</nav>