<div class="body-load" id="preloader">
  <div class="loader" style="--bg: #14544d">
    <div class="dot" style="--index: 0;"></div>
    <div class="dot" style="--index: 1;"></div>
    <div class="dot" style="--index: 2;"></div>
    <div class="dot" style="--index: 3;"></div>
    <div class="dot" style="--index: 4;"></div>
    <div class="dot" style="--index: 5;"></div>
    <div class="dot" style="--index: 6;"></div>
    <div class="dot" style="--index: 7;"></div>
  </div>
</div>
