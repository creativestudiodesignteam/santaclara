
<footer class="footer">
        <div class="newsletter">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 text-center">
                        <h1 class="title">Newsletter</h1>
                        <p class="description">Fique por dentro das novidades.</p>
                        <form class="form-inline d-flex justify-content-center">
                            <label class="sr-only" for="inlineFormInputName2">E-mail</label>
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="E-mail">
                            
                            <label class="sr-only" for="inlineFormInputName2">Nome</label>
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Nome">

                            <button type="submit" class="btn-solid-green mb-2">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="informations">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="text-center">
                            <img src="assets/images/clara.png" width="150px" class="img-fluid" />
                            <div class="links d-none d-xl-block">
                                <a href="resort.php">O Resort</a>
                                <a href="acomodacoes.php">Acomodações</a>
                                <a href="atividades-lazer.php">Atividades e lazer</a>
                                <a href="criancas-adultos.php">de crianças a adultos</a>
                                <a href="promocao.php">Promoções e pacotes</a>
                                <a href="eventos.php">Eventos</a>
                                <a href="gastronomia.php">gastronomia</a>
                                <a href="contato.php">Fale conosco</a>
                            </div>
                        </div>

                        <div class="row remove-xl mt-4">
                            <div class="col-6 d-flex justify-content-center">
                                <div class="links-2">
                                    <a href="resort.php">O Resort</a>
                                    <a href="acomodacoes.php">Acomodações</a>
                                    <a href="atividades-lazer.php">Atividades e lazer</a>
                                    <a href="criancas-adultos.php">de crianças a adultos</a>
                                </div>
                            </div>

                            <div class="col-6 d-flex justify-content-center">
                                <div class="links-2">
                                    <a href="promocao.php">Promoções e pacotes</a>
                                    <a href="eventos.php">Eventos</a>
                                    <a href="gastronomia.php">gastronomia</a>
                                    <a href="contato.php">Fale conosco</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="social">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 offset-xl-3 text-center align-self-center">
                        <a href="#." class="whats"><i class="flaticon-whatsapp"></i> (16) 98174-7159</a>
                    </div>
                    <div class="col-xl-2 align-self-center">
                        <ul class="list-inline list-unstyled social-list">
                            <li class="list-inline-item">
                                <a href="https://www.facebook.com/santaclaraecoresort" target="_BLANK"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="https://www.instagram.com/clararesorts/" target="_BLANK"><i class="fab fa-instagram"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="https://www.youtube.com/user/santaclaraeco" target="_BLANK"><i class="fab fa-youtube"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="https://www.tripadvisor.com.br/Hotel_Review-g3590955-d1969234-Reviews-Santa_Clara_Eco_Resort-Dourado_State_of_Sao_Paulo.html" target="_BLANK"><i class="fab fa-tripadvisor"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="space d-block d-xl-none"></div>