<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home">
        <?php 
        $label = "Para todas as idades";
        include("includes/menu.php"); ?>
        
        <?php 
        $title = "Para todas as idades";
        include("includes/top-bar.php"); ?>
    </header>

    <section class="leisure">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <div class="card-deck card-actives">
                        <a href="interna-atividades.php" class="card position-relative">
                            <img class="card-img-top" src="assets/images/escalada.jpg" class="img-fluid"/>
                            
                            <div class="row no-gutters label">
                                <span class="label-bar">Aventura</span>
                            </div>

                            <div class="card-body">
                                <h1 class="card-title m-0">Escalada</h1>
                            </div>
                        </a>

                        <a href="interna-atividades.php" class="card position-relative">
                            <img class="card-img-top" src="assets/images/escalada.jpg" class="img-fluid"/>
                            
                            <div class="row no-gutters label">
                                <span class="label-bar">Aventura</span>
                            </div>

                            <div class="card-body">
                                <h1 class="card-title m-0">Escalada</h1>
                            </div>
                        </a>

                        <a href="interna-atividades.php" class="card position-relative">
                            <img class="card-img-top" src="assets/images/escalada.jpg" class="img-fluid"/>
                            
                            <div class="row no-gutters label">
                                <span class="label-bar">Aventura</span>
                            </div>

                            <div class="card-body">
                                <h1 class="card-title m-0">Escalada</h1>
                            </div>
                        </a>

                        <a href="interna-atividades.php" class="card position-relative">
                            <img class="card-img-top" src="assets/images/escalada.jpg" class="img-fluid"/>
                            
                            <div class="row no-gutters label">
                                <span class="label-bar">Aventura</span>
                            </div>

                            <div class="card-body">
                                <h1 class="card-title m-0">Escalada</h1>
                            </div>
                        </a>
                    </div>

                    <div class="card-deck card-actives mt-4">
                        <a href="interna-atividades.php" class="card position-relative">
                            <img class="card-img-top" src="assets/images/escalada.jpg" class="img-fluid"/>
                            
                            <div class="row no-gutters label">
                                <span class="label-bar">Aventura</span>
                            </div>

                            <div class="card-body">
                                <h1 class="card-title m-0">Escalada</h1>
                            </div>
                        </a>

                        <a href="interna-atividades.php" class="card position-relative">
                            <img class="card-img-top" src="assets/images/escalada.jpg" class="img-fluid"/>
                            
                            <div class="row no-gutters label">
                                <span class="label-bar">Aventura</span>
                            </div>

                            <div class="card-body">
                                <h1 class="card-title m-0">Escalada</h1>
                            </div>
                        </a>

                        <a href="interna-atividades.php" class="card position-relative">
                            <img class="card-img-top" src="assets/images/escalada.jpg" class="img-fluid"/>
                            
                            <div class="row no-gutters label">
                                <span class="label-bar">Aventura</span>
                            </div>

                            <div class="card-body">
                                <h1 class="card-title m-0">Escalada</h1>
                            </div>
                        </a>

                        <a href="interna-atividades.php" class="card position-relative">
                            <img class="card-img-top" src="assets/images/escalada.jpg" class="img-fluid"/>
                            
                            <div class="row no-gutters label">
                                <span class="label-bar">Aventura</span>
                            </div>

                            <div class="card-body">
                                <h1 class="card-title m-0">Escalada</h1>
                            </div>
                        </a>
                    </div>

                    <div class="card-deck card-actives mt-4">
                        <a href="interna-atividades.php" class="card position-relative">
                            <img class="card-img-top" src="assets/images/escalada.jpg" class="img-fluid"/>
                            
                            <div class="row no-gutters label">
                                <span class="label-bar">Aventura</span>
                            </div>

                            <div class="card-body">
                                <h1 class="card-title m-0">Escalada</h1>
                            </div>
                        </a>

                        <a href="interna-atividades.php" class="card position-relative">
                            <img class="card-img-top" src="assets/images/escalada.jpg" class="img-fluid"/>
                            
                            <div class="row no-gutters label">
                                <span class="label-bar">Aventura</span>
                            </div>

                            <div class="card-body">
                                <h1 class="card-title m-0">Escalada</h1>
                            </div>
                        </a>

                        <a href="interna-atividades.php" class="card position-relative">
                            <img class="card-img-top" src="assets/images/escalada.jpg" class="img-fluid"/>
                            
                            <div class="row no-gutters label">
                                <span class="label-bar">Aventura</span>
                            </div>

                            <div class="card-body">
                                <h1 class="card-title m-0">Escalada</h1>
                            </div>
                        </a>

                        <a href="interna-atividades.php" class="card position-relative">
                            <img class="card-img-top" src="assets/images/escalada.jpg" class="img-fluid"/>
                            
                            <div class="row no-gutters label">
                                <span class="label-bar">Aventura</span>
                            </div>

                            <div class="card-body">
                                <h1 class="card-title m-0">Escalada</h1>
                            </div>
                        </a>
                    </div>

                    
                    <a href="#." class="btn-outline-green d-inline-block mt-4" id="loadmore">Carregar mais</a>
                    <!-- <p class='semmais' id='semmais'>Sem mais</p> -->
                    <a href="#." class="btn-outline-green mt-4" id="loadless">Mostrar menos</a>
                </div>
            </div>
        </div>
    </section>

    <?php include("includes/accommodations.php")?>
    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
        /* $(function () {
            "use strict";
            $('.card-actives').slice(0, 1).css("display","flex");

            $('#loadmore').on('click', function (e) {
                e.preventDefault();
                $('.card-actives:hidden').slice(0, 2).css("display","flex");
                if ($('.card-actives:hidden').length === 0) {
                    $('#loadmore').replaceWith("<p class='p'>Sem mais</p>");
                }
            });
        }); */

        $(function () {
            "use strict";
            
            $('.card-actives').slice(0, 1).css("display","flex");
            var mostrar = 4; // quantos quer mostrar e esconder

            $('#loadmore, #loadless').on('click', function (e) {
                e.preventDefault();
                
                if(this.id == "loadmore"){
                
                    $('#loadless').show().css("display","inline-block"); // exibe o botão "mostrar menos"
                    
                    // mostra mais 4 cards
                    $('.card-actives:hidden').slice(0, mostrar).css("display","flex");
                    
                    // se não houver mais nenhum card escondido
                    // esconde o botão "loadmore" e mostra o "sem mais"
                    if (!$('.card-actives:hidden').length) {
                        $('#loadmore').hide();
                        $('#semmais').show();
                    }
                }else{
                    // esconde os últimos 4 cards visíveis
                    $('.card-actives:visible').slice(1, mostrar).hide();
                    
                    // se não houver nenhum card visível
                    // esconde o botão "mostrar menos"
                    if (!$('.card-actives:visible').length) {
                        $('#loadless').hide();
                    }
                    
                    // se houver pelo menos um card escondido
                    // mostra o botão "loadmore" e esconde o "sem mais"
                    if($('.card-actives:hidden').length){
                        $('#loadmore').show();
                        $('#semmais').hide();
                    }
                    
                }
            });
        });
    </script>
</body>
</html>