<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home header-infra">
        <?php 
        $label = "Infraestrutura";
        include("includes/menu.php"); ?>
        
        <?php 
        $title = "O Resort > Infraestrutura";
        include("includes/top-bar.php"); ?>
    </header>

    <section class="infra">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Duis vitae aliquet ante, vitae accumsan libero. </h1>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. 
                    </p>
                </div>
            </div>
            
            <div class="packages-infra">
                <div class="container">
                    <div class="row pacs">
                        <div class="col-xl-12 text-center">
                            <div class="block -one">
                                <p class="name m-0">5 pscinas</p>
                                <p class="m-0 desc">uma de água mineral corrente</p>
                            </div>
        
                            <div class="block -two">
                                <p class="name m-0">5 pscinas</p>
                                <p class="m-0 desc">uma de água mineral corrente</p>
                            </div>

                            <div class="block -three">
                                <p class="name m-0">5 pscinas</p>
                                <p class="m-0 desc">uma de água mineral corrente</p>
                            </div>
        
                            <div class="block -one">
                                <p class="name m-0">5 pscinas</p>
                                <p class="m-0 desc">uma de água mineral corrente</p>
                            </div>
        
                            <div class="block -two">
                                <p class="name m-0">5 pscinas</p>
                                <p class="m-0 desc">uma de água mineral corrente</p>
                            </div>

                            <div class="block -three">
                                <p class="name m-0">5 pscinas</p>
                                <p class="m-0 desc">uma de água mineral corrente</p>
                            </div>

                            <div class="block -one">
                                <p class="name m-0">5 pscinas</p>
                                <p class="m-0 desc">uma de água mineral corrente</p>
                            </div>
        
                            <div class="block -two">
                                <p class="name m-0">5 pscinas</p>
                                <p class="m-0 desc">uma de água mineral corrente</p>
                            </div>

                            <div class="block -three">
                                <p class="name m-0">5 pscinas</p>
                                <p class="m-0 desc">uma de água mineral corrente</p>
                            </div>

                            <div class="block -one">
                                <p class="name m-0">5 pscinas</p>
                                <p class="m-0 desc">uma de água mineral corrente</p>
                            </div>
        
                            <div class="block -two">
                                <p class="name m-0">5 pscinas</p>
                                <p class="m-0 desc">uma de água mineral corrente</p>
                            </div>

                            <div class="block -three">
                                <p class="name m-0">5 pscinas</p>
                                <p class="m-0 desc">uma de água mineral corrente</p>
                            </div>
                        </div>
                    </div>
        
                </div>
            </div>
        </div>
    </section>

    <section class="about-infra">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h2 class="title-master">Conheça a infraestrutura</h2>
                    <div class="swiper-container swiper-infra">
                        <div class="swiper-counter"></div>
                        <div class="arrows">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>

                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                               <div class="gallery-infra">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12 text-center">
                                                <h1 class="title">Pscinas</h1>
                                                <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. </p>
                                                <div class="gallery">
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>

                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                               <div class="gallery-infra">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12 text-center">
                                                <h1 class="title">Pscinas</h1>
                                                <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. </p>
                                                <div class="gallery">
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>

                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                               <div class="gallery-infra">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12 text-center">
                                                <h1 class="title">Pscinas</h1>
                                                <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. </p>
                                                <div class="gallery">
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>

                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    

    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html> 