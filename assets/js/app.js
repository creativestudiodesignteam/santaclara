/* $(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
        $(".navbar-mobile").addClass("fixed-mobile");
    }
});  */

/* $(function() {
   
    var header = $(".navbar-mobile");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 0) {
            header.removeClass('navbar-mobile').addClass("fixed-mobile");
        } else {
            header.removeClass("fixed-mobile").addClass('navbar-mobile');
        }
    });
}); */

$(function () {$("#preloader").fadeOut();});

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll > 0) {
        $(".navbar-mobile").addClass("fixed-mobile");
    } else{
        $(".navbar-mobile").removeClass("fixed-mobile");
    }
});

function openNav() {
    document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}

var swiper = new Swiper('.swiper-home', {
    loop: true,
    simulateTouch: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },

    scrollbar: {
        container: '.swiper-scrollbar',
        hide: false,
        draggable: true,
        snapOnRelease: true
    },
  });


    console.clear();

    /**
     * Update of secondary numeric pagination
     * @this {object}  - swiper instance
     */
    function updSwiperNumericPagination() {
    this.el.querySelector( '.swiper-counter' )
        .innerHTML = '<span class="count">'+ (this.realIndex + 1) +'</span>/<span class="total">'+ this.el.slidesQuantity +'</span>';
    }


    document.addEventListener( 'DOMContentLoaded', function () {
        document.querySelectorAll( '.swiper-activities' ).forEach( function( node ) {
            // Getting slides quantity before slider clones them
            node.slidesQuantity = node.querySelectorAll( '.swiper-slide' ).length;
            
            // Swiper initialization
            new Swiper( node, {
                speed:         1000,
                loop:          true,
                autoplay:      { delay: 100000, },
                pagination:    { el: node.querySelector('.swiper-pagination') },
                
                on: { // Secondary pagination is update after initialization and after slide change
                    init:        updSwiperNumericPagination,
                    slideChange: updSwiperNumericPagination
                },

                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        });
    });


    console.clear();

    /**
     * Update of secondary numeric pagination
     * @this {object}  - swiper instance
     */
    function updSwiperNumericPagination() {
    this.el.querySelector( '.swiper-counter' )
        .innerHTML = '<span class="count">'+ (this.realIndex + 1) +'</span>/<span class="total">'+ this.el.slidesQuantity +'</span>';
    }


    document.addEventListener( 'DOMContentLoaded', function () {
        document.querySelectorAll( '.swiper-child' ).forEach( function( node ) {
            // Getting slides quantity before slider clones them
            node.slidesQuantity = node.querySelectorAll( '.swiper-slide' ).length;
            
            // Swiper initialization
            new Swiper( node, {
                speed:         1000,
                loop:          true,
                autoplay:      { delay: 7000, },
                pagination:    { el: node.querySelector('.swiper-pagination') },
                
                on: { // Secondary pagination is update after initialization and after slide change
                    init:        updSwiperNumericPagination,
                    slideChange: updSwiperNumericPagination
                },

                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        });
    });
    

    var swiper = new Swiper('.swiper-testmonial', {
        loop: true,
        simulateTouch: true,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
    
        pagination: {
            el: '.swiper-pagination-test',
            clickable: true,
        },
    
        scrollbar: {
            container: '.swiper-scrollbar',
            hide: false,
            draggable: true,
            snapOnRelease: true
        },
      });

      var swiper = new Swiper('.swiper-baby', {
        loop: true,
        simulateTouch: true,
        speed:         1000,
        autoplay:      { delay: 5000, },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
    
        pagination: {
            el: '.swiper-pagination-baby',
            clickable: true,
        },
    
        scrollbar: {
            container: '.swiper-scrollbar',
            hide: false,
            draggable: true,
            snapOnRelease: true
        },
      });


    console.clear();

    /**
     * Update of secondary numeric pagination
     * @this {object}  - swiper instance
     */
    function updSwiperNumericPagination() {
    this.el.querySelector( '.swiper-counter' )
        .innerHTML = '<span class="count">'+ (this.realIndex + 1) +'</span>/<span class="total">'+ this.el.slidesQuantity +'</span>';
    }


    document.addEventListener( 'DOMContentLoaded', function () {
        document.querySelectorAll( '.swiper-events' ).forEach( function( node ) {
            // Getting slides quantity before slider clones them
            node.slidesQuantity = node.querySelectorAll( '.swiper-slide' ).length;
            
            // Swiper initialization
            new Swiper( node, {
                speed:         1000,
                loop:          true,
                autoplay:      { delay: 10000, },
                pagination:    { el: node.querySelector('.swiper-pagination') },
                
                on: { // Secondary pagination is update after initialization and after slide change
                    init:        updSwiperNumericPagination,
                    slideChange: updSwiperNumericPagination
                },

                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        });
    });



    console.clear();

    /**
     * Update of secondary numeric pagination
     * @this {object}  - swiper instance
     */
    function updSwiperNumericPagination() {
    this.el.querySelector( '.swiper-counter' )
        .innerHTML = '<span class="count">'+ (this.realIndex + 1) +'</span>/<span class="total">'+ this.el.slidesQuantity +'</span>';
    }


    document.addEventListener( 'DOMContentLoaded', function () {
        document.querySelectorAll( '.swiper-eventos-01' ).forEach( function( node ) {
            // Getting slides quantity before slider clones them
            node.slidesQuantity = node.querySelectorAll( '.swiper-slide' ).length;
            
            // Swiper initialization
            new Swiper( node, {
                speed:         1000,
                loop:          true,
                autoplay:      { delay: 10000, },
                pagination:    { el: node.querySelector('.swiper-pagination') },
                
                on: { // Secondary pagination is update after initialization and after slide change
                    init:        updSwiperNumericPagination,
                    slideChange: updSwiperNumericPagination
                },

                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        });
    });


    console.clear();

    /**
     * Update of secondary numeric pagination
     * @this {object}  - swiper instance
     */
    function updSwiperNumericPagination() {
    this.el.querySelector( '.swiper-counter' )
        .innerHTML = '<span class="count">'+ (this.realIndex + 1) +'</span>/<span class="total">'+ this.el.slidesQuantity +'</span>';
    }


    document.addEventListener( 'DOMContentLoaded', function () {
        document.querySelectorAll( '.swiper-infra' ).forEach( function( node ) {
            // Getting slides quantity before slider clones them
            node.slidesQuantity = node.querySelectorAll( '.swiper-slide' ).length;
            
            // Swiper initialization
            new Swiper( node, {
                speed:         1000,
                loop:          true,
                autoplay:      { delay: 10000, },
                pagination:    { el: node.querySelector('.swiper-pagination') },
                
                on: { // Secondary pagination is update after initialization and after slide change
                    init:        updSwiperNumericPagination,
                    slideChange: updSwiperNumericPagination
                },

                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        });
    });


    console.clear();

    /**
     * Update of secondary numeric pagination
     * @this {object}  - swiper instance
     */
    function updSwiperNumericPagination() {
    this.el.querySelector( '.swiper-counter' )
        .innerHTML = '<span class="count">'+ (this.realIndex + 1) +'</span>/<span class="total">'+ this.el.slidesQuantity +'</span>';
    }


    document.addEventListener( 'DOMContentLoaded', function () {
        document.querySelectorAll( '.swiper-family' ).forEach( function( node ) {
            // Getting slides quantity before slider clones them
            node.slidesQuantity = node.querySelectorAll( '.swiper-slide' ).length;
            
            // Swiper initialization
            new Swiper( node, {
                speed:         1000,
                loop:          true,
                autoplay:      { delay: 10000, },
                pagination:    { el: node.querySelector('.swiper-pagination') },
                
                on: { // Secondary pagination is update after initialization and after slide change
                    init:        updSwiperNumericPagination,
                    slideChange: updSwiperNumericPagination
                },

                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        });
    });


    console.clear();

    /**
     * Update of secondary numeric pagination
     * @this {object}  - swiper instance
     */
    function updSwiperNumericPagination() {
    this.el.querySelector( '.swiper-counter' )
        .innerHTML = '<span class="count">'+ (this.realIndex + 1) +'</span>/<span class="total">'+ this.el.slidesQuantity +'</span>';
    }


    document.addEventListener( 'DOMContentLoaded', function () {
        document.querySelectorAll( '.swiper-program' ).forEach( function( node ) {
            // Getting slides quantity before slider clones them
            node.slidesQuantity = node.querySelectorAll( '.swiper-slide' ).length;
            
            // Swiper initialization
            new Swiper( node, {
                speed:         1000,
                loop:          true,
                autoplay:      { delay: 1000000, },
                pagination:    { el: node.querySelector('.swiper-pagination') },
                
                on: { // Secondary pagination is update after initialization and after slide change
                    init:        updSwiperNumericPagination,
                    slideChange: updSwiperNumericPagination
                },

                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        });
    });

    var $doc = $('html, body');
    $('.scroll').click(function() {
        $doc.animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
        return false;
    });

    jQuery(document).ready(function($){
	    
        $(".btnrating").on('click',(function(e) {
        
        var previous_value = $("#selected_rating").val();
        
        var selected_value = $(this).attr("data-attr");
        $("#selected_rating").val(selected_value);
        
        $(".selected-rating").empty();
        $(".selected-rating").html(selected_value);
        
        for (i = 1; i <= selected_value; ++i) {
        $("#rating-star-"+i).toggleClass('btn-warning');
        $("#rating-star-"+i).toggleClass('btn-default');
        }
        
        for (ix = 1; ix <= previous_value; ++ix) {
        $("#rating-star-"+ix).toggleClass('btn-warning');
        $("#rating-star-"+ix).toggleClass('btn-default');
        }
        
        }));
    });
    

    $(document).ready(function() {
        $('#youtubeVideo').on('hidden.bs.modal', function() {
          var $this = $(this).find('iframe'),
            tempSrc = $this.attr('src');
          $this.attr('src', "");
          $this.attr('src', tempSrc);
        });
    });
    