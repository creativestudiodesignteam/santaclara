<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home">
        <?php 
        $label = "Fale conosco";
        include("includes/menu.php"); ?>
        

        <?php 
        $title = "Fale conosco";
        include("includes/top-bar.php"); ?>
    </header>

    <section class="contato">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. 
                    </p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-xl-4 offset-xl-2 box">
                    <div class="row">
                        <div class="col-xl-2 align-self-center">
                            <i class="fab fa-whatsapp"></i>
                        </div>
                        <div class="col-xl-10 align-self-center">
                            <a href="#.">
                                <p class="title m-0">Whatsapp</p>    
                                <p class="number m-0">(16) 98174-7159</p>    
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 box">
                    <div class="row">
                        <div class="col-xl-2 align-self-center">
                            <i class="fas fa-envelope"></i>
                        </div>
                        <div class="col-xl-10 align-self-center">
                            <a href="#.">
                                <p class="title m-0">E-mail</p>    
                                <p class="number m-0">contato@santaclaraecoresorts.com.br</p>    
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 offset-xl-2 box">
                    <div class="row">
                        <div class="col-xl-2 align-self-center">
                            <i class="fas fa-mobile-alt"></i>
                        </div>
                        <div class="col-xl-10 align-self-center">
                            <a href="#.">
                                <p class="title m-0">Celular</p>    
                                <p class="number m-0">(16) 98174-7159 / (16) 98192-5960<br>(16) 98191-0550 / (16) 98191-1621 </p>    
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 box">
                    <div class="row">
                        <div class="col-xl-2 align-self-center">
                            <i class="fas fa-phone"></i>
                        </div>
                        <div class="col-xl-10 align-self-center">
                            <a href="#.">
                                <p class="title m-0">Telefone</p>    
                                <p class="number m-0">(16) 98174-7159 / (16) 98192-5960<br>(16) 98191-0550 / (16) 98191-1621 </p>    
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-xl-12 text-center">
                    <h2 class="title">Eventos</h2>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-xl-4 offset-xl-2 box">
                    <div class="row">
                        <div class="col-xl-2 align-self-center">
                            <i class="fab fa-whatsapp"></i>
                        </div>
                        <div class="col-xl-10 align-self-center">
                            <a href="#.">
                                <p class="title m-0">Whatsapp</p>    
                                <p class="number m-0">(16) 98174-7159</p>    
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 box">
                    <div class="row">
                        <div class="col-xl-2 align-self-center">
                            <i class="fas fa-envelope"></i>
                        </div>
                        <div class="col-xl-10 align-self-center">
                            <a href="#.">
                                <p class="title m-0">E-mail</p>    
                                <p class="number m-0">contato@santaclaraecoresorts.com.br</p>    
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-xl-8 offset-xl-2 text-center">
                    <h2 class="title">Envie sua mensagem</h2>
                    <form>
                        <div class="form-row">
                            <div class="form-group col-xl-6">
                                <input type="text" class="form-control" id="inputEmail4" placeholder="Nome">
                            </div>

                            <div class="form-group col-xl-6">
                                <input type="phone" class="form-control" id="inputPassword4" placeholder="Telefone">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-xl-12">
                                <input type="email" class="form-control" id="inputEmail4" placeholder="e-mail">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-xl-12">
                                <textarea name="message" class="form-control" rows="2" placeholder="Mensagem"></textarea>
                            </div>
                        </div>
                        <a href="#" class="btn-outline-green">Enviar</a>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html> 