<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home header-promocao-01">
        <?php 
        $label = "pacotes";
        include("includes/menu.php"); ?>
        

        <?php 
        $title = "Pacotes";
        include("includes/top-bar.php"); ?>
    </header>

    <section class="packages">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. 
                    </p>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Pacote Spa</h1>
                </div>
            </div>

            <div class="row pacs">
                <div class="col-xl-6 text-center">
                    <h3 class="title-info">Day Spa</h3>
                    <p class="hour">Das <span class="hour-one">10h às 13h</span> ou <span class="hour-one"> 15 às 18h*</span></p>
                    <div class="block -one">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>


                    <div class="block -two">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>

                    <div class="block -one">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>


                    <div class="block -two">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>

                    <div class="block -color">
                        <p class="valor m-0">Valores: R$ 295,00 + taxas</p>
                    </div>

                    <p class="informations">
                    *Não incluso refeições e estrutura lazer do Resort.<br>
                    *Bebidas e extras consumidas ou utilizadas no período deverão ser pagas no checkout.<br>
                    *Lugares limitados, é necessário reservar com antecedência.
                    </p>
                </div>

                <div class="col-xl-6 text-center">
                    <h3 class="title-info">Day Spa Plus</h3>
                    <p class="hour">Das <span class="hour-one">10h às 13h</span> ou <span class="hour-one"> 15 às 18h*</span></p>
                    <div class="block -one">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>


                    <div class="block -two">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>

                    <div class="block -one">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>


                    <div class="block -two">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>

                    <div class="block -color">
                        <p class="valor m-0">Valores: R$ 295,00 + taxas</p>
                    </div>

                    <p class="informations">
                    *Não incluso refeições e estrutura lazer do Resort.<br>
                    *Bebidas e extras consumidas ou utilizadas no período deverão ser pagas no checkout.<br>
                    *Lugares limitados, é necessário reservar com antecedência.
                    </p>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Pacote Spa</h1>
                </div>
            </div>

            <div class="row pacs">
                <div class="col-xl-6 text-center">
                    <h3 class="title-info">Day Spa</h3>
                    <p class="hour">Das <span class="hour-one">10h às 13h</span> ou <span class="hour-one"> 15 às 18h*</span></p>
                    <div class="block -one">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>


                    <div class="block -two">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>

                    <div class="block -one">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>


                    <div class="block -two">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>

                    <div class="block -color">
                        <p class="valor m-0">Valores: R$ 295,00 + taxas</p>
                    </div>

                    <p class="informations">
                    *Não incluso refeições e estrutura lazer do Resort.<br>
                    *Bebidas e extras consumidas ou utilizadas no período deverão ser pagas no checkout.<br>
                    *Lugares limitados, é necessário reservar com antecedência.
                    </p>
                </div>

                <div class="col-xl-6 text-center">
                    <h3 class="title-info">Day Spa Plus</h3>
                    <p class="hour">Das <span class="hour-one">10h às 13h</span> ou <span class="hour-one"> 15 às 18h*</span></p>
                    <div class="block -one">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>


                    <div class="block -two">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>

                    <div class="block -one">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>


                    <div class="block -two">
                        <p class="name m-0">Massagem</p>
                        <p class="m-0 desc">Relax, Revitalizante ou D.Tissue</p>
                    </div>

                    <div class="block -color">
                        <p class="valor m-0">Valores: R$ 295,00 + taxas</p>
                    </div>

                    <p class="informations">
                    *Não incluso refeições e estrutura lazer do Resort.<br>
                    *Bebidas e extras consumidas ou utilizadas no período deverão ser pagas no checkout.<br>
                    *Lugares limitados, é necessário reservar com antecedência.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html> 