<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home header-family">
        <?php 
        $label = "Família";
        include("includes/menu.php"); ?>
        
        <?php 
        $title = "O Resort > Cuidados para família";
        include("includes/top-bar.php"); ?>
    </header>

    <section class="family">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Duis vitae aliquet ante, vitae accumsan libero. </h1>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. 
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="about-family">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h2 class="title-master">Conheça os cuidados especiais</h2>
                    <div class="swiper-container swiper-family">
                        <div class="swiper-counter"></div>
                        <div class="arrows">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>

                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                               <div class="gallery-infra">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12 text-center">
                                                <h1 class="title">No quarto</h1>
                                                <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. </p>
                                                <div class="gallery">
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>

                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                               <div class="gallery-infra">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12 text-center">
                                                <h1 class="title">No quarto</h1>
                                                <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. </p>
                                                <div class="gallery">
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>

                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                               <div class="gallery-infra">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12 text-center">
                                                <h1 class="title">No quarto</h1>
                                                <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. </p>
                                                <div class="gallery">
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>

                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                                    <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                                    <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html> 