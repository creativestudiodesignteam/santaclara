<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home header-sustentabilidade">
        <?php 
        $label = "Sustentabilidade";
        include("includes/menu.php"); ?>
        
        <?php 
        $title = "Sustentabilidade e Social";
        include("includes/top-bar.php"); ?>
    </header>

    <section class="sustentabilidade">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Sustentabilidade</h1>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. 
                    </p>

                    <div class="gallery mt-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12 text-center">
                                    <div class="gallery">
                                        <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                        <div class="clear"></div>

                                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                        <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="social-01">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Ações Sociais</h1>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. 
                    </p>

                    <div class="gallery mt-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12 text-center">
                                    <div class="gallery">
                                        <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                        <div class="clear"></div>

                                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title="Beautiful Image"/></a>
                                        <a href="assets/images/03.jpg" class="big"><img src="assets/images/thumbs/03.jpg" alt="" title="Beautiful Image" /></a>
                                        <a href="assets/images/03.jpg"><img src="assets/images/thumbs/01.jpg" alt="" title=""/></a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html> 