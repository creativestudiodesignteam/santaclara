<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home">
        <?php 
        $label = "Cavalgada";
        include("includes/menu.php"); ?>
        

        <?php 
        $title = "Programações";
        include("includes/top-bar.php"); ?>
    </header>

    <section class="all-program">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                <div id="demo-2">
                    <div id="one-2" class="pb-5"></div>
                </div>
                    <h1 class="title">
                        Programação de Quinta feira - 13/08
                    </h1>
                    <div class="center-elements">
                        <div class="box-one-program">
                            <div class="f-aux">
                                <div class="text-center">
                                    <h3 class="title-day">manhã</h3>
                                    
                                    <a href="#." class="box-program mb-2">
                                        <span class="hour">08:00</span>
                                        <p class="aula m-0">Aula de equitação</p>
                                        <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                    </a>
                                    <a href="#." class="box-program mb-2">
                                        <span class="hour">08:00</span>
                                        <p class="aula m-0">Aula de equitação</p>
                                        <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                    </a>
                                    <a href="#." class="box-program mb-2">
                                        <span class="hour">08:00</span>
                                        <p class="aula m-0">Aula de equitação</p>
                                        <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="box-one-program">
                            <div class="f-aux">
                                <div class="text-center">
                                    <h3 class="title-day">tarde</h3>
                                    
                                    <a href="#." class="box-program mb-2">
                                        <span class="hour">08:00</span>
                                        <p class="aula m-0">Aula de equitação</p>
                                        <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                    </a>
                                    <a href="#." class="box-program mb-2">
                                        <span class="hour">08:00</span>
                                        <p class="aula m-0">Aula de equitação</p>
                                        <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                    </a>
                                    <a href="#." class="box-program mb-2">
                                        <span class="hour">08:00</span>
                                        <p class="aula m-0">Aula de equitação</p>
                                        <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="box-one-program">
                            <div class="f-aux">
                                <div class="text-center">
                                    <h3 class="title-day">noite</h3>
                                    
                                    <a href="#." class="box-program mb-2">
                                        <span class="hour">08:00</span>
                                        <p class="aula m-0">Aula de equitação</p>
                                        <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                    </a>
                                    <a href="#." class="box-program mb-2">
                                        <span class="hour">08:00</span>
                                        <p class="aula m-0">Aula de equitação</p>
                                        <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                    </a>
                                    <a href="#." class="box-program mb-2">
                                        <span class="hour">08:00</span>
                                        <p class="aula m-0">Aula de equitação</p>
                                        <span class="local">Onde: Saída da cocheira/Fazendinha</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html> 